#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include "PILAdinamica.h"

void crearArchivoTS(void);
int cargarEnTS(char*,int);
void asignarValorConstante(void);
void crearArchivoTercertos(void);
int CargarEnTercetos(char*, char*, char*);
void CargarEnTercetoValorDesapilado(int x, char* t2);
void negarTerceto(int);
char* comparadorOpuesto(char* str);
void asignarTipo(t_pila* p);
//enum tipoDato {VACIO,INT,CTE_INT, REAL, CTE_REAL, STRING, CTE_STRING,ID};
char tipo[9][14]={"","ENTERO", "CTE ENTERA","REAL","CTE REAL","STRING","CTE STRING","ID", "CONST"};

typedef struct {
    char nombre[100]; // Nombre del token
    char tipo[14];  // Tipo de Dato
    int  tipoDato; // Manejo interno del tipo tipo[14]
    int flag;  // Para saber si el token fue almacenado o no en la TS
    char valor[100];
} tsimbolo; // tabla de simbolos


// Tabla de simbolos
tsimbolo simbolo[100];

typedef struct {
    char t1[100];
    char t2[100];
    char t3[100];
    int flag;
} tterceto;

tterceto tercetos[500];
// Funciones generales de la TS ------------------------------------------------------------------------

void crearArchivoTS(void) {
	FILE *fp;
	int x, i;
	fp = fopen ( "ts.txt", "w+" );
	if (fp == NULL) {
		fputs ("File error",stderr); 
		exit (1);
	}
	    
    fprintf(fp, "NOMBRE %93s | TIPO %9s | VALOR %94s\n", " ", " ", " ");
    for (i=0; i<220; i++)
        fprintf(fp, "-");
    fprintf(fp, "\n");   
    
	for (x = 0; x < 100; x++)
    {
        if( simbolo[x].tipoDato != 0 )
            fprintf(fp, "%-100s | %-14s | %-100s\n", simbolo[x].nombre, simbolo[x].tipo, simbolo[x].valor);
        else
            break;
    		
	}
    /*
	for (x = 0; x < 100; x++)
    {
        if( simbolo[x].tipoDato != 0 )
            fprintf(fp, "%s\t%s\t%s\n", simbolo[x].nombre, simbolo[x].tipo, simbolo[x].valor);
        else
            break;
    		
	}*/
	fclose(fp);
	
    printf("\n\nSe ha cerrado el archivo y la Tabla de Simbolos fue cargada sin errores.\n");
        
 }

int cargarEnTS ( char *nombre, int val ){
    int x;
	int l_repetido=0;
    char nombreConGuion[strlen(nombre)+1];    

    for (x=0; x<100; x++ ){
        if (simbolo[x].flag==1){//para saber si el token ya esta en la tabla
            if (strcmp (nombre,simbolo[x].nombre)==0){
                return x;
                
            }
        }
    }
        
    for (x=0; x<100 ; x++){
        if(simbolo[x].flag==0){

            if(strstr(tipo[val],"CTE")){
                
                strcpy(nombreConGuion, "_");   
                strcat(nombreConGuion, nombre);
                strcpy(simbolo[x].nombre,nombreConGuion);
                strcpy(simbolo[x].valor,nombre);
            }else{
                strcpy(simbolo[x].nombre,nombre);
            }
            
            strcpy(simbolo[x].tipo,tipo[val]);
            simbolo[x].tipoDato=val;
            simbolo[x].flag=1;//para indicar que ya se almaceno en la tabla

            return x;
        }
    }
		
	return x;
 }//retorna posicion en la tabla de simbolos
 
 void asignarValorConstante()
 {
     int x;
     for (x=0; x<99; x++ ){
        if (simbolo[x].flag==1){//para saber si el token ya esta en la tabla
            if (simbolo[x].tipoDato==8){
                char *aux=simbolo[x+1].nombre;
                strcpy(simbolo[x].valor,++aux);//valor reconocido                
            }
        }
    }
 }

void asignarTipo(t_pila *pilaTipos)
 {
     int x;
     for (x=0; x<99; x++ ){
        if (simbolo[x].flag==0){//busca último token cargado
            break;
        }
    }

    int tipoDato;
    --x;
    for (; x>=0; x--){// recorro el array a la inversa a medida que desapilo el tipo de dato
        desapilar(pilaTipos,&tipoDato);
        simbolo[x].tipoDato=tipoDato;
        strcpy(simbolo[x].tipo,tipo[tipoDato]);
    }
 }


void crearArchivoTercetos(void) {
	FILE *fp;
	int x, i;
	fp = fopen ( "intermedia.txt", "w+" );
	if (fp == NULL) {
		fputs ("File error",stderr); 
		exit (1);
	}
	    
    fprintf(fp, "ID | T1 | T2 | T3\n");
    for (i=0; i<220; i++)
        fprintf(fp, "-");
    fprintf(fp, "\n");   
    
	for (x = 0; x < 500; x++)
    {
        if(tercetos[x].flag == 1)
        {
            fprintf(fp, "%d | %-10s | %-10s | %-10s\n", x, tercetos[x].t1, tercetos[x].t2, tercetos[x].t3);
        }
	}

	fclose(fp);
        
 }

int CargarEnTercetos(char *t1, char *t2, char *t3)
{
    int x;
    for (x=0; x<100 ; x++)
    {
        if(tercetos[x].flag==0)
        {
            strcpy(tercetos[x].t1,t1);
            strcpy(tercetos[x].t2,t2);
            strcpy(tercetos[x].t3,t3);
            tercetos[x].flag=1;
            break;
        }
    }
    return x;
}

void CargarEnTercetoValorDesapilado(int x, char* t2)
{
    strcpy(tercetos[x].t2,t2);
}

void negarTerceto(int nroTerceto)
{
    strcpy(tercetos[nroTerceto].t1,comparadorOpuesto(tercetos[nroTerceto].t1));
}

char* comparadorOpuesto(char* str)
{
    char *retorno = malloc(100 * sizeof(char));
    retorno[0] = '\0'; 

    if( !strcmp(str, "BLE") )
        strcpy(retorno, "BGT");
    if( !strcmp(str, "BGT") )
        strcpy(retorno, "BLE");
    if( !strcmp(str, "BLT") )
        strcpy(retorno, "BGE");
    if( !strcmp(str, "BGE") )
        strcpy(retorno, "BLT");
    if( !strcmp(str, "BNE") )
        strcpy(retorno, "BQE");
    if( !strcmp(str, "BQE") )
        strcpy(retorno, "BNE");

    return retorno;    
}