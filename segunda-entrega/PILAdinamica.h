#ifndef PILADINAMICA_H_INCLUDED
#define PILADINAMICA_H_INCLUDED
#include <stdio.h>
#include <stdlib.h>
#define PILA_LLENA 0
#define PILA_VACIA 0
#define TODO_BIEN 33
#define TAM 100

typedef struct s_nodo{
    int dato;
    struct s_nodo* sig;
}t_nodo;

typedef t_nodo* t_pila;

/// PROTOTIPOS DE FUNCIONES ///

void crear_pila( t_pila* p );
int pila_vacia( const t_pila* p );
int pila_llena( const t_pila* p );
void vaciar_pila( t_pila* p );
int apilar( t_pila* p, const int* d );
int desapilar( t_pila* p, int* d );
int ver_tope(const t_pila* p,int *d);

void crear_pila(t_pila* p)
{
    *p=NULL;
}

int pila_vacia(const t_pila* p)
{
    return *p==NULL;
}

int pila_llena(const t_pila* p)
{
    void* nodoAux = malloc(sizeof(t_nodo));
    free(nodoAux);
    return nodoAux==NULL;
}

void vaciar_pila(t_pila* p)
{
    t_nodo* aux;
    while(*p)
    {
        aux=*p;
        *p=(*p)->sig;
        free(aux);
    }
}
int apilar(t_pila *p ,const int* d)
{
    t_nodo* nuevo;
    nuevo = (t_nodo*)malloc(sizeof(t_nodo));
    if(!nuevo)
        return 0;
    nuevo->dato= *d;
    nuevo->sig= *p;
    *p = nuevo;
    return 1;
}

int desapilar(t_pila* p,int* d)
{
    t_nodo* aux;
    if(*p==NULL)
        return PILA_VACIA;
    aux = *p;
    *d = (*p)->dato;
    *p = (*p)->sig;
    free(aux);
    return TODO_BIEN;
}

int ver_tope(const t_pila* p, int* d)
{
    if(!(*p))
        return PILA_VACIA;
    *d=(*p)->dato;
    return TODO_BIEN;
}

#endif // PILADINAMICA_H_INCLUDED

