include macros.asm
include number.asm

.MODEL LARGE
.386
.STACK 200h
.DATA

contador 	dd ?
promedio 	dd ?
actual 	dd ?
suma 	dd ?
@int4 	dd 85.00
nombre 	dd 85
@int6 	dd 2.00
pivot 	dd 2
@str8 	db "Prueba.txt", "$", 30 dup (?)
@str9 	db "Prueba.txt LyC Tema 3!", "$", 30 dup (?)
@str10 	db "Ingrese un valor entero para actual: ", "$", 30 dup (?)
@int11 	dd 0.00
@float12 	dd 2.50
@int13 	dd 9.00
@int14 	dd 1.00
@float15 	dd 0.34
@str16 	db "La suma es: ", "$", 30 dup (?)
@str17 	db "actual es mayor que 2 y distinto de cero", "$", 30 dup (?)
@str18 	db "no es mayor que 2", "$", 30 dup (?)
@max1	dd ?
@aux0	dd ?
@aux1	dd ?
@aux2	dd ?
@aux3	dd ?
@aux4	dd ?
@aux5	dd ?
@aux6	dd ?
@aux7	dd ?
@aux8	dd ?
@aux9	dd ?
@aux10	dd ?
@aux11	dd ?
@aux12	dd ?
@aux13	dd ?
@aux14	dd ?
@aux15	dd ?
@aux16	dd ?
@aux17	dd ?
@aux18	dd ?
@aux19	dd ?
@aux20	dd ?
@aux21	dd ?
@aux22	dd ?
@aux23	dd ?
@aux24	dd ?
@aux25	dd ?
@aux26	dd ?
@aux27	dd ?
@aux28	dd ?
@aux29	dd ?
@aux30	dd ?
@aux31	dd ?
@aux32	dd ?
@aux33	dd ?
@aux34	dd ?
@aux35	dd ?
@aux36	dd ?
@aux37	dd ?
@aux38	dd ?
@aux39	dd ?
@aux40	dd ?
@aux41	dd ?
@aux42	dd ?
@aux43	dd ?
@aux44	dd ?
@aux45	dd ?
@aux46	dd ?
@aux47	dd ?
@aux48	dd ?
@aux49	dd ?
@aux50	dd ?
@aux51	dd ?
@aux52	dd ?
@aux53	dd ?
@aux54	dd ?
@aux55	dd ?
@aux56	dd ?
@aux57	dd ?
@aux58	dd ?
@aux59	dd ?
@aux60	dd ?
@aux61	dd ?
@aux62	dd ?
@aux63	dd ?
@aux64	dd ?
@aux65	dd ?
@aux66	dd ?
@aux67	dd ?
@aux68	dd ?
@aux69	dd ?
@aux70	dd ?
@aux71	dd ?
@aux72	dd ?
@aux73	dd ?
@aux74	dd ?
@aux75	dd ?
@aux76	dd ?
@aux77	dd ?
@aux78	dd ?
@aux79	dd ?
@aux80	dd ?
@aux81	dd ?
@aux82	dd ?
@aux83	dd ?

.CODE

START:
MOV EAX, @DATA
MOV DS, EAX
MOV ES, EAX

fld @int4
fstp @aux0
fld nombre
fstp @aux1
fld @aux0
fstp nombre
fld @int6
fstp @aux3
fld pivot
fstp @aux4
fld @aux3
fstp pivot
MOV  SI, OFFSET @str8
displayString @str8
newLine
MOV  SI, OFFSET @str9
displayString @str9
newLine
MOV  SI, OFFSET @str10
displayString @str10
newLine
fld actual
fstp @aux12
getFloat actual,2
fld contador
fstp @aux14
fld @int11
fstp @aux15
fld @aux15
fstp contador
fld suma
fstp @aux17
fld @float12
fstp @aux18
fld nombre
fstp @aux19
fld @aux18
fld @aux19
fadd
fstp @aux20
fld @aux20
fstp suma
jump_22:
fld contador
fstp @aux23
fld @int13
fstp @aux24
fld contador
fld @int13
fxch
fcomp
ffree St(0)
fstsw ax
sahf
ja jump_63
fld contador
fstp @aux27
fld contador
fstp @aux28
fld @int14
fstp @aux29
fld @aux28
fld @aux29
fadd
fstp @aux30
fld @aux30
fstp contador
fld actual
fstp @aux32
fld contador
fstp @aux33
fld @float15
fstp @aux34
fld @aux33
fld @aux34
fdiv
fstp @aux35
fld contador
fstp @aux36
fld actual
fstp @aux38
fld contador
fstp @aux39
fld @aux38
fld @aux39
fmul
fstp @aux40
fld @aux40
fstp @max1
fld @int6
fstp @aux42
fld @max1
fld @int6
fxch
fcomp
ffree St(0)
fstsw ax
sahf
jae jump_46
fld @aux42
fstp @max1
jump_46:
fld actual
fstp @aux46
fld pivot
fstp @aux47
fld @aux46
fld @aux47
fmul
fstp @aux48
fld @max1
fld @aux48
fxch
fcomp
ffree St(0)
fstsw ax
sahf
jae jump_52
fld @aux48
fstp @max1
jump_52:
fld @aux46
fld @max1
fmul
fstp @aux52
fld @aux35
fld @aux52
fadd
fstp @aux53
fld @aux53
fstp actual
fld suma
fstp @aux55
fld suma
fstp @aux56
fld actual
fstp @aux57
fld @aux56
fld @aux57
fadd
fstp @aux58
fld @aux58
fstp suma
fld suma
fstp @aux60
displayFloat suma,2
newLine
jmp jump_22
jump_63:
MOV  SI, OFFSET @str16
displayString @str16
newLine
fld suma
fstp @aux65
displayFloat suma,2
newLine
fld actual
fstp @aux67
fld @int6
fstp @aux68
fld actual
fld @int6
fxch
fcomp
ffree St(0)
fstsw ax
sahf
jna jump_78
fld actual
fstp @aux71
fld @int11
fstp @aux72
fld actual
fld @int11
fxch
fcomp
ffree St(0)
fstsw ax
sahf
je jump_78
MOV  SI, OFFSET @str17
displayString @str17
newLine
jmp jump_84
jump_78:
fld actual
fstp @aux78
fld nombre
fstp @aux79
fld actual
fld nombre
fxch
fcomp
ffree St(0)
fstsw ax
sahf
jae jump_84
MOV  SI, OFFSET @str18
displayString @str18
newLine
jump_84:

MOV EAX, 4C00h
INT 21h

END START
