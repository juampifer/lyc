#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdio.h>
#include "PILAdinamica.h"

void crearArchivoTS(void);
int cargarEnTS(char*,int);
void asignarValorConstante(void);
void crearArchivoTercetos(void);
int CargarEnTercetos(char*, char*, char*);
void CargarEnTercetoValorDesapilado(int x, char* t2);
void negarTerceto(int);
char* comparadorOpuesto(char* str);
void asignarTipo(t_pila* p);
int buscarTipoID(char*);
int buscarTipoDatoPorID(char*);
int existeVariable( char*);
int buscarIndiceTSPorValor(char* );
int buscarIndiceTSPorNombre(char* );

void generarASM(void);
void generarCodigo (FILE*);
char * tipoDatoASM(int);
void cargarDATA(FILE*);
int tipoTerceto(tterceto);
char *substring(char *, int , int );
int detectarCorchete(char *);

int jumps[500];


//enum tipoDato {VACIO,INT,CTE_INT, REAL, CTE_REAL, STRING, CTE_STRING,ID};
char tipo[11][14]={"","ENTERO","CTE ENTERA","REAL","CTE REAL","STRING","CTE STRING","ID","CONST ENTERA","CONST REAL","CONST STRING"};
typedef struct {
    char nombre[100]; // Nombre del token
    char tipo[14];  // Tipo de Dato
    int  tipoDato; // Manejo interno del tipo tipo[14]
    int flag;  // Para saber si el token fue almacenado o no en la TS
    char valor[100];
} tsimbolo; // tabla de simbolos


// Tabla de simbolos
tsimbolo simbolo[100];

typedef struct {
    char t1[100];
    char t2[100];
    char t3[100];
    int flag;
} tterceto;

tterceto tercetos[500];
t_pila pilaEtiqueta;

// Funciones generales de la TS ------------------------------------------------------------------------

void crearArchivoTS(void) {
	FILE *fp;
	int x, i;
	fp = fopen ( "ts.txt", "w+" );
	if (fp == NULL) {
		fputs ("File error",stderr); 
		exit (1);
	}
	    
    fprintf(fp, "NOMBRE %93s | TIPO %9s | VALOR %94s\n", " ", " ", " ");
    for (i=0; i<220; i++)
        fprintf(fp, "-");
    fprintf(fp, "\n");   
    
	for (x = 0; x < 100; x++)
    {
        if( simbolo[x].tipoDato != 0 )
            fprintf(fp, "%-100s | %-14s | %-100s\n", simbolo[x].nombre, simbolo[x].tipo, simbolo[x].valor);
        else
            break;
    		
	}
	fclose(fp);
	
    printf("\n\nSe ha cerrado el archivo y la Tabla de Simbolos fue cargada sin errores.\n");
        
 }

int cargarEnTS ( char *nombre, int val ){
    int x;
	int l_repetido=0;
    char nombreConGuion[strlen(nombre)+1];    

    for (x=0; x<100; x++ ){
        if (simbolo[x].flag==1){//para saber si el token ya esta en la tabla
            if (strcmp (nombre,simbolo[x].nombre)==0){
                if( simbolo[x].valor == 0 ){
                    printf("ERROR! Variable duplicada");
                    exit(0);
                }

                return x;
                
            }else{
                strcpy(nombreConGuion, "_");   
                strcat(nombreConGuion, nombre);
                if (strcmp (nombreConGuion,simbolo[x].nombre)==0){
                    return x;
                }
            }
        }
    }
        
    for (x=0; x<100 ; x++){
        if(simbolo[x].flag==0){

            if(strstr(tipo[val],"CTE")){
                
                strcpy(nombreConGuion, "_");   
                strcat(nombreConGuion, nombre);
                strcpy(simbolo[x].nombre,nombreConGuion);
                strcpy(simbolo[x].valor,nombre);
            }else{
                strcpy(simbolo[x].nombre,nombre);
            }
            
            strcpy(simbolo[x].tipo,tipo[val]);
            simbolo[x].tipoDato=val;
            simbolo[x].flag=1;//para indicar que ya se almaceno en la tabla

            return x;
        }
    }
		
	return x;
 }//retorna posicion en la tabla de simbolos
 
 void asignarValorConstante()
 {
     int x;
     for (x=0; x<99; x++ ){
        if (simbolo[x].flag==1){//para saber si el token ya esta en la tabla
            if (simbolo[x].tipoDato==8 || simbolo[x].tipoDato==9 || simbolo[x].tipoDato==10){
                char *aux=simbolo[x-1].nombre;
                strcpy(simbolo[x].valor,++aux);//valor reconocido                
            }
        }
    }
 }

void asignarTipo(t_pila *pilaTipos)
{
    int x;
    for (x=0; x<99; x++ ){
        if (simbolo[x].flag==0){//busca último token cargado
            break;
        }
    }
    int tipoDato;
    --x;
    for (; x>=0; x--){// recorro el array a la inversa a medida que desapilo el tipo de dato
        if( desapilar(pilaTipos,&tipoDato) == 0 ){
            break;
        }
        simbolo[x].tipoDato=tipoDato;
        strcpy(simbolo[x].tipo,tipo[tipoDato]);
    }
}

int buscarTipoID(char* nombre)
{
    int x;
    for (x=0; x<99; x++ ){
        if ( (strcmp(simbolo[x].valor, nombre) == 0 || strcmp(simbolo[x].nombre, nombre) == 0) && simbolo[x].flag==1)
        {
            return simbolo[x].tipoDato;
        }
    }
    return -1;
}

int buscarTipoDatoPorID(char* nombre)
{
    int x;
    for (x=0; x<99; x++ ){
        if ( strcmp(simbolo[x].nombre, nombre) == 0 && simbolo[x].flag==1)
        {
            return simbolo[x].tipoDato;
        }
    }
    return -1;
}

int buscarIndiceTSPorValor(char* nombre)
{
    int x;
    for (x=0; x<99; x++ ){
        if ( strcmp(simbolo[x].valor, nombre) == 0 && simbolo[x].flag==1)
        {
            return x;
        }
    }
    return -1;
}


int buscarIndiceTSPorNombre(char* nombre)
{
    int x;
    for (x=0; x<99; x++ ){
        if ( strcmp(simbolo[x].nombre, nombre) == 0 && simbolo[x].flag==1)
        {
            return x;
        }
    }
    return -1;
}


int existeVariable( char* nombre){
    int x;
    for (x=0; x<99; x++ ){
        if ( strcmp(simbolo[x].nombre, nombre) == 0 && simbolo[x].flag==1)
        {
            return 0;
        }
    }
    return -1;
}

// Funciones generales para Tercetos ------------------------------------------------------------------------
void crearArchivoTercetos(void) {
	FILE *fp;
	int x, i;
	fp = fopen ( "intermedia.txt", "w+" );
	if (fp == NULL) {
		fputs ("File error",stderr); 
		exit (1);
	}
	    
    fprintf(fp, "ID | T1 | T2 | T3\n");
    for (i=0; i<220; i++)
        fprintf(fp, "-");
    fprintf(fp, "\n");   
    
	for (x = 0; x < 500; x++)
    {
        if(tercetos[x].flag == 1)
        {
            fprintf(fp, "%d | %-10s | %-10s | %-10s\n", x, tercetos[x].t1, tercetos[x].t2, tercetos[x].t3);
        }
	}

	fclose(fp);
}

int CargarEnTercetos(char *t1, char *t2, char *t3)
{
    int x;
    for (x=0; x<100 ; x++)
    {
        if(tercetos[x].flag==0)
        {
            strcpy(tercetos[x].t1,t1);
            strcpy(tercetos[x].t2,t2);
            strcpy(tercetos[x].t3,t3);
            tercetos[x].flag=1;
            break;
        }
    }
    return x;
}

void CargarEnTercetoValorDesapilado(int x, char* t2)
{
    strcpy(tercetos[x].t2,t2);
}

void negarTerceto(int nroTercetoT2)
{
    strcpy(tercetos[nroTercetoT2].t1,comparadorOpuesto(tercetos[nroTercetoT2].t1));
}

char* comparadorOpuesto(char* str)
{
    char *retorno = malloc(100 * sizeof(char));
    retorno[0] = '\0'; 

    if( !strcmp(str, "BLE") )
        strcpy(retorno, "BGT");
    if( !strcmp(str, "BGT") )
        strcpy(retorno, "BLE");
    if( !strcmp(str, "BLT") )
        strcpy(retorno, "BGE");
    if( !strcmp(str, "BGE") )
        strcpy(retorno, "BLT");
    if( !strcmp(str, "BNE") )
        strcpy(retorno, "BQE");
    if( !strcmp(str, "BQE") )
        strcpy(retorno, "BNE");

    return retorno;    
}

void generarAsm(void) {
	FILE *fp;
	int x, i;
	fp = fopen ( "Final.asm", "w+" );
	if (fp == NULL) {
		fputs ("File error",stderr); 
		exit (1);
	}
	
    fprintf(fp, "include macros.asm\n");
    fprintf(fp, "include number.asm\n\n");          
    fprintf(fp, ".MODEL LARGE\n");
    fprintf(fp, ".386\n");
    fprintf(fp, ".STACK 200h\n");
    cargarDATA(fp);
    generarCodigo(fp);
    fprintf(fp, "\n");
    fprintf(fp, "MOV EAX, 4C00h\n");
    fprintf(fp, "INT 21h\n\n");
    fprintf(fp, "END START");
    fprintf(fp, "\n");
	fclose(fp);
	
    printf("\n\nFin generacion codigo asm.\n");
        
 }

 void generarCodigo (FILE* fp) {   
     int nroTercetoT2; 
     int nroTercetoT3; 
     char nombreConGuion[100];
     int tope;
    fprintf(fp, "\n.CODE\n\n");
    fprintf(fp, "START:\n");
    fprintf(fp, "MOV EAX, @DATA\n");
    fprintf(fp, "MOV DS, EAX\n");
    fprintf(fp, "MOV ES, EAX\n\n");
    int x=0;
     for (x = 0; tercetos[x].flag == 1; x++) 
     {

         ver_tope(&pilaEtiqueta,&tope);


         if(jumps[x]==1){             
             fprintf(fp, "jump_%d:\n", x);
             //desapilar(&pilaEtiqueta,&tope);
         }

         nroTercetoT2=detectarCorchete(tercetos[x].t2);
         nroTercetoT3=detectarCorchete(tercetos[x].t3);
         switch (tipoTerceto(tercetos[x]))
         {
            case 1://es un id 
                //fprintf(fp, "MOV @aux%d,%s\n", x,tercetos[x].t1);
                fprintf(fp, "fld %s\n", tercetos[x].t1);
                fprintf(fp, "fstp @aux%d\n",x);
            break;
            case 2:// es cte
                //fprintf(fp, "MOV @aux%d,%s\n", x,tercetos[x].t1);
                switch (buscarTipoID(tercetos[x].t1))
                {
                    case 2: fprintf(fp, "fld @int%d\n", buscarIndiceTSPorValor(tercetos[x].t1)); break;
                    case 4: fprintf(fp, "fld @float%d\n", buscarIndiceTSPorValor(tercetos[x].t1)); break;
                    //case 6: fprintf(fp, "mov  SI, OFFSET @str%d\n", buscarIndiceTSPorValor(tercetos[x].t1)); break;
                }
                fprintf(fp, "fstp @aux%d\n",x);
            break;

            case 3://es operador:=
                    switch (buscarTipoDatoPorID(tercetos[nroTercetoT2].t1))
                    { 
                        case 5:
                             fprintf(fp, "MOV  DI, OFFSET %s\n", tercetos[nroTercetoT2].t1);
                             fprintf(fp, "STRCPY\n");
                            ; break;    
                        default:
                            if(strcmp(tercetos[nroTercetoT2].t1, "@max1") == 0) 
                            {
                                fprintf(fp, "fld @aux%d\n",nroTercetoT3);
                                fprintf(fp, "fstp %s\n",tercetos[nroTercetoT2].t1);
                            }  
                            else  if(strcmp(tercetos[nroTercetoT3].t1, "@max1") == 0) 
                            {
                                fprintf(fp, "fld %s\n",tercetos[nroTercetoT3].t1);
                                fprintf(fp, "fstp %s\n",tercetos[nroTercetoT2].t1);
                            }    
                            else
                            {
                                if(nroTercetoT2!=-1){
                                    fprintf(fp, "fld @aux%d\n",nroTercetoT3);
                                    fprintf(fp, "fstp %s\n",tercetos[nroTercetoT2].t1);
                                }else{
                                    fprintf(fp, "fld %s\n",tercetos[x].t3);
                                    fprintf(fp, "fstp %s\n",tercetos[x].t2);
                                }
                            };                            
                            
                            break;  
                    }                           
             break;

            case 4://op +
                if(strcmp(tercetos[nroTercetoT2].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT2!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT2);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t2);
                } 
                
                 if(strcmp(tercetos[nroTercetoT3].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT3!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT3);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t3);
                }
                fprintf(fp, "fadd\n");
                fprintf(fp, "fstp @aux%d\n", x);
             break;   

            case 5://op *
                if(strcmp(tercetos[nroTercetoT2].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT2!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT2);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t2);
                } 
                
                if(strcmp(tercetos[nroTercetoT3].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT3!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT3);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t3);
                }
                fprintf(fp, "fmul\n");
                fprintf(fp, "fstp @aux%d\n", x);
             break;   

            case 6://op -
                if(strcmp(tercetos[nroTercetoT2].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT2!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT2);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t2);
                } 
                
                if(strcmp(tercetos[nroTercetoT3].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT3!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT3);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t3);
                }
                fprintf(fp, "fsub\n");                
                fprintf(fp, "fstp @aux%d\n", x);
             break;                                       
         
            case 7://op /
                if(strcmp(tercetos[nroTercetoT2].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT2!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT2);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t2);
                } 
                
                if(strcmp(tercetos[nroTercetoT3].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(nroTercetoT3!=-1){
                    fprintf(fp, "fld @aux%d\n",nroTercetoT3);
                }else{
                    fprintf(fp, "fld %s\n",tercetos[x].t3);
                }
                fprintf(fp, "fdiv\n");                
                fprintf(fp, "fstp @aux%d\n", x);
             break;

            case 8://Palabra reservada PUT

            switch (buscarTipoID(tercetos[nroTercetoT2].t1))
                {
                case 1:
                case 2:                    
                    if(nroTercetoT2!=-1){
                        fprintf(fp, "displayFloat %s,0\n",tercetos[nroTercetoT2].t1);
                    }else{
                        fprintf(fp, "displayFloat %s,0\n",tercetos[x].t2);
                    } 
                    break;                      
                case 3:
                case 4:
                    if(nroTercetoT2!=-1){
                        fprintf(fp, "displayFloat %s,2\n",tercetos[nroTercetoT2].t1);
                    }else{
                        fprintf(fp, "displayFloat %s,2\n",tercetos[x].t2);
                    } 
                    break;                    
                case 5:
                case 6:
                    if(nroTercetoT2!=-1){
                        if(buscarTipoDatoPorID(tercetos[nroTercetoT2].t1) == 5)
                        {
                            fprintf(fp, "displayString %s\n",tercetos[nroTercetoT2].t1);
                        }
                        else
                        {
                            fprintf(fp, "displayString @str%d\n",buscarIndiceTSPorValor(tercetos[nroTercetoT2].t1));
                        }
                        
                    }else{
                        fprintf(fp, "displayString %s\n",tercetos[x].t2);
                    } 
                    break;

                default:
                    break;
                }      
                fprintf(fp, "newLine\n");          
             break;   

            case 9://Palabra reservada CMP
                if(strcmp(tercetos[nroTercetoT2].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(buscarTipoID(tercetos[nroTercetoT2].t1) == 2)
                {
                    fprintf(fp, "fld @int%d\n", buscarIndiceTSPorValor(tercetos[nroTercetoT2].t1));
                }
                else if(buscarTipoID(tercetos[nroTercetoT2].t1) == 4)
                {
                    fprintf(fp, "fld @float%d\n", buscarIndiceTSPorValor(tercetos[nroTercetoT2].t1));
                }
                else if(buscarTipoID(tercetos[nroTercetoT2].t1) == 6)
                {
                    fprintf(fp, "fld @str%d\n", buscarIndiceTSPorValor(tercetos[nroTercetoT2].t1));
                }
                else if(buscarTipoID(tercetos[nroTercetoT2].t1) > 0)
                {
                    fprintf(fp, "fld %s\n", tercetos[nroTercetoT2].t1);
                }
                else
                {
                    fprintf(fp, "fld @aux%d\n", nroTercetoT2);
                }
                
                if(strcmp(tercetos[nroTercetoT3].t1, "@max1") == 0) 
                {
                    fprintf(fp, "fld @max1\n");
                }
                else if(buscarTipoID(tercetos[nroTercetoT3].t1) == 2)
                {
                    fprintf(fp, "fld @int%d\n", buscarIndiceTSPorValor(tercetos[nroTercetoT3].t1));
                }
                else if(buscarTipoID(tercetos[nroTercetoT3].t1) == 4)
                {
                    fprintf(fp, "fld @float%d\n", buscarIndiceTSPorValor(tercetos[nroTercetoT3].t1));
                }
                else if(buscarTipoID(tercetos[nroTercetoT3].t1) == 6)
                {
                    fprintf(fp, "fld @str%d\n", buscarIndiceTSPorValor(tercetos[nroTercetoT3].t1));
                }
                else if(buscarTipoID(tercetos[nroTercetoT3].t1) > 0)
                {                    
                    fprintf(fp, "fld %s\n", tercetos[nroTercetoT3].t1);
                }                
                else
                {
                    fprintf(fp, "fld @aux%d\n", nroTercetoT3);
                }
                fprintf(fp, "fxch\n");
                fprintf(fp, "fcomp\n");
                fprintf(fp, "ffree St(0)\n");                
                fprintf(fp, "fstsw ax\n");
                fprintf(fp, "sahf\n");
             break;

            case 10:
                fprintf(fp, "jna jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break; 

            case 11:
                fprintf(fp, "jmp jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break;   

            case 12:
                fprintf(fp, "jump_%d:\n",x);
                jumps[x]=1;
            break;  

            case 13:
                fprintf(fp, "ja jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break; 

            case 14:
                fprintf(fp, "jb jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break; 

            case 15:
                fprintf(fp, "jae jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break; 

            case 16:
                fprintf(fp, "jne jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break; 

            case 17:
                fprintf(fp, "je jump_%d\n",nroTercetoT2);
                jumps[nroTercetoT2]=1;
            break;

            case 18:
                switch (buscarTipoDatoPorID(tercetos[nroTercetoT2].t1))
                {
                case 1:
                case 2:                    
                    if(nroTercetoT2!=-1){
                        fprintf(fp, "getFloat %s,0\n",tercetos[nroTercetoT2].t1);
                    }else{
                        fprintf(fp, "getFloat %s,0\n",tercetos[x].t2);
                    } 
                    break;                      
                case 3:
                case 4:
                    if(nroTercetoT2!=-1){
                        fprintf(fp, "getFloat %s,2\n",tercetos[nroTercetoT2].t1);
                    }else{
                        fprintf(fp, "getFloat %s,2\n",tercetos[x].t2);
                    } 
                    break;                    
                case 5:
                case 6:
                    if(nroTercetoT2!=-1){
                        if(buscarTipoDatoPorID(tercetos[nroTercetoT2].t1) == 5)
                        {
                            fprintf(fp, "getString @str%d\n",buscarIndiceTSPorNombre(tercetos[nroTercetoT2].t1));
                        }
                        else
                        {
                            fprintf(fp, "getString @str%d\n",buscarIndiceTSPorValor(tercetos[nroTercetoT2].t1));
                        }
                        
                    }else{
                        fprintf(fp, "getString %s\n",tercetos[x].t2);
                    } 
                    break;

                default:
                    break;
                }               
             break;

            case 19://es cte string
                fprintf(fp, "MOV  SI, OFFSET @str%d\n", buscarIndiceTSPorValor(tercetos[x].t1));
                break;
            case 20:// es id string
                // fprintf(fp, "MOV  SI, OFFSET %s\n", tercetos[x].t1);
                //fprintf(fp, "MOV  DI, OFFSET @aux%d\n",x);                
                break;              
         default:
             break;
         }
     }
    if(jumps[x]==1){             
        fprintf(fp, "jump_%d:\n", x);
    }
 }

 void cargarDATA(FILE* fp){
    fprintf(fp, ".DATA\n\n");

    for (int x = 0; x < 100; x++)
    {
        if( simbolo[x].flag == 1 )
        {
            /*if(strlen(simbolo[x].valor)==0){//es un id
                fprintf(fp,  "%s \t%s ?\n", simbolo[x].nombre, tipoDatoASM(simbolo[x].tipoDato));
            }else{*/
                if(simbolo[x].tipoDato == 2)
                {
                    fprintf(fp,  "@int%d \t%s %.2f\n", x, tipoDatoASM(simbolo[x].tipoDato),atof(simbolo[x].valor));
                }
                else if(simbolo[x].tipoDato == 4)
                {
                    fprintf(fp,  "@float%d \t%s %.2f\n", x, tipoDatoASM(simbolo[x].tipoDato),atof(simbolo[x].valor));
                }
                else if(simbolo[x].tipoDato == 6)
                {
                    fprintf(fp,  "@str%d \t%s %s, \"$\", 30 dup (?)\n", x, tipoDatoASM(simbolo[x].tipoDato),simbolo[x].valor);
                }
                else if(simbolo[x].tipoDato == 5){
                    fprintf(fp,  "%s \t%s %s 30 dup (?), \"$\"\n", simbolo[x].nombre, tipoDatoASM(simbolo[x].tipoDato),simbolo[x].valor);
                }
                else if(simbolo[x].tipoDato == 8 || simbolo[x].tipoDato == 9 || simbolo[x].tipoDato == 10){
                    fprintf(fp,  "%s \t%s %s\n", simbolo[x].nombre, tipoDatoASM(simbolo[x].tipoDato),simbolo[x].valor);
                }
                else{
                    fprintf(fp,  "%s \t%s ?\n", simbolo[x].nombre, tipoDatoASM(simbolo[x].tipoDato));
                }
                
            //}
            
        }else{
            break;
        }    		
	}

     fprintf(fp,  "@max1\tdd ?\n");

    for (int x = 0; x < 500; x++)
    {
        if(tercetos[x].flag==1){
            fprintf(fp,  "@aux%d\tdd ?\n",x);
        }else{
            break;
        }
        
    }

 }

 char * tipoDatoASM(int tipoDato){
     if(tipoDato==5||tipoDato==6){
        return "db";
     }else{
        return "dd";
     }
 }

 int tipoTerceto(tterceto terceto){
     //return:
     //1-cuando es ID
     //2-cuando es cte numerica
     //3- op :=
     //4- op +
     //5- op *
     //6- op -
     //7- op /
     //8-PUT
     //9-CMP
     //10-BLE
     //11-BI
     //12-ET
     //13-BGT
     //14-BLT
     //15-BGE
     //16-BNE
     //17-BQE
     //18-GET
     //19-cte tipo string
     //20-id tipo string

    for (int x=0; x<99; x++ ){
        char *aux;
        char nombreConGuion[strlen(terceto.t1)+1];        
        strcpy(nombreConGuion, "_"); 
        strcat(nombreConGuion, terceto.t1);

        if ( strcmp(simbolo[x].nombre, terceto.t1) == 0 && simbolo[x].flag==1)
        {
            switch (simbolo[x].tipoDato)
                {
                case 5:
                case 6:
                    return 20;
                    break;
                
                default:
                    return 1;
                    break;
                }
        }else{
            if ( strcmp(simbolo[x].nombre, nombreConGuion) == 0 && simbolo[x].flag==1)
            {
                switch (simbolo[x].tipoDato)
                {
                case 5:
                case 6:
                    return 19;
                    break;
                
                default:
                    return 2;
                    break;
                }
            }
        }

        if(strcmp(terceto.t1,":=")==0){
            return 3;
        }

        if(strcmp(terceto.t1,"+")==0){
            return 4;
        }

        if(strcmp(terceto.t1,"*")==0){
            return 5;
        }        

        if(strcmp(terceto.t1,"-")==0){
            return 6;
        }

        if(strcmp(terceto.t1,"/")==0){
            return 7;
        }  

        if(strcmp(terceto.t1,"PUT")==0){
            return 8;
        } 

        if(strcmp(terceto.t1,"CMP")==0){
            return 9;
        }

        if(strcmp(terceto.t1,"BLE")==0){
            return 10;
        }                        

        if(strcmp(terceto.t1,"BI")==0){
            return 11;
        }  

        if(strcmp(terceto.t1,"ET")==0){
            return 12;
        }

        if(strcmp(terceto.t1,"BGT")==0){
            return 13;
        }
        if(strcmp(terceto.t1,"BLT")==0){
            return 14;
        }
        if(strcmp(terceto.t1,"BGE")==0){
            return 15;
        }        
        if(strcmp(terceto.t1,"BNE")==0){
            return 16;
        }
        if(strcmp(terceto.t1,"BQE")==0){
            return 17;
        } 
        if(strcmp(terceto.t1,"GET")==0){
            return 18;
        } 

    }

    return -1;

 }
//devuelve el nro de terceto
 int detectarCorchete(char * contenido){
    if(contenido[0]=='['){
        return atoi(substring(contenido,2,strlen(contenido)-2));
    }else{
        return -1;
    }
 }

char *substring(char *string, int position, int length)
{
   char *p;
   int c;
   p = malloc((length+1));
   if (p == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(1);
   }
   for (c = 0; c < length; c++)
   {
      *(p+c) = *(string+position-1);      
      string++;  
   }
 
   *(p+c) = '\0';
 
   return p;
}