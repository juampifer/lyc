%{
/*---------------------------INCLUDES---------------------------*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "y.tab.h"
#include "tabla_simbolos.h"
#include "PILAdinamica.h"

/*-------------------DECLARACION DE VARIABLES-------------------*/

int constanteIndice = 0; //Variables para acciones semanticas.
int factorIndice = 0;
int terminoIndice = 0;
int terminoIndice1 = 0;
int expresionIndice = 0;
int expresionIndice1 = 0;
int lizquierdoIndice = 0;
int lderechoIndice = 0;
int comparacionIndice = 0;
int comparadorIndice = 0;
int condicionIndice = 0;
int asignacionIndice = 0;
int sentenciaIndice = 0;
int seleccionIndice = 0;
int programaIndice = 0;
int bloqueIndice = 0;
int entradaIndice = 0;
int salidaIndice = 0;
int mensajeIndice = 0;
int iteracionIndice = 0;
int idIndice = 0;
int listaIndice = 0;
int maximoIndice = 0;
int indiceConst = 0;

int tipoAux = -1; //Variables para manejar tipos de datos.
int tipoFactor = -1;
int tipoTermino = -1;
int tipoExpresion = -1;
int tipoConstante = -1;
int tipoID=-1;

int cantVariables = 0; //Variables para controlar declaraciones.
int cantTipos = 0;

char tipoComparador[4]; //Variable auxiliar para comparadores.
int nroMax = 1; //Variable auxiliar para maximo.

/*-------------------VARIABLES DE BISON-------------------*/
int yystopparser=0;
FILE  *yyin;
int yyerror();
int yylex();

/*-------------------DECLARACION DE FUNCIONES-------------------*/
char* ConvertirAString(int);
void procesarIfAnd(int);
void procesarIfOr(int,int);
char* obtenerNombreVariable();

//Tipos {0 = "",1 = "ENTERO",2 = "CTE ENTERA",3 = "REAL",4 = "CTE REAL",5 = "STRING",6 = "CTE STRING",7 = "ID",8 = "CONST",8 = "CONST ENTERA", 9 = "CONST REAL", 10 = "CONST STRING"}​​​​​;

t_pila pilaIF;
t_pila pilaWHILE;
t_pila pilaMAXIMO;
t_pila pilaTIPO;

%}

%union {
    int int_val;
    double float_val;
    char *str_val;
}

%type <str_val> ID CTE_STRING
%type <int_val> CTE_INT
%type <float_val> CTE_REAL

%token RES_DIM
%token RES_AS
%token RES_WHILE
%token RES_IF
%token RES_AND
%token RES_OR
%token RES_NOT
%token RES_PUT
%token RES_GET
%token RES_ELSE
%token RES_TIPO_ENTERO
%token RES_TIPO_REAL
%token RES_TIPO_STRING
%token RES_CONST
%token RES_MAXIMO

%token ARROBA
%token ESP
%token COM
%token NUMERAL
%token PREGUNTA_ABRE
%token PREGUNTA_CIERRA
%token COMENTARIO_ABRE
%token COMENTARIO_CIERRA
%token COMA
%token PUNTO
%token SIGN
%token BLOQUE_ABRE
%token BLOQUE_CIERRA
%token CORCHETE_ABRE
%token CORCHETE_CIERRA

%token PAR_ABRE
%token PAR_CIERRA
%token ADMIRACION_ABRE
%token ADMIRACION_CIERRA

%token FIN_SENTENCIA

%token ID

%token OP_COMP_IGUAL
%token OP_COMP_MAYOR_IGUAL
%token OP_COMP_MAYOR
%token OP_COMP_MENOR
%token OP_COMP_MENOR_IGUAL
%token OP_COMP_DIST

%token OP_ASIG
%token OP_COCIENTE
%token OP_MULT
%token OP_SUMA
%token OP_RESTA
%token OP_PORCENTAJE

%token COMENTARIO

%token CTE_STRING
%token CTE_REAL
%token CTE_INT


%token LISTA_DE_VARIABLES
%token LISTA_DE_TIPOS

%token DECLARACION_LISTA_VARIABLES

%%
p:
    programa {
        printf("\n\nFIN PROGRAMA");
        asignarValorConstante(); 
        crearArchivoTS(); 
        crearArchivoTercetos();
        generarAsm();
    } ;

programa: 
    bloque { programaIndice = bloqueIndice;};

bloque:
    sentencia
    {
        bloqueIndice = sentenciaIndice;
    }|
    bloque sentencia 
    {
        bloqueIndice = sentenciaIndice;
    };

sentencia:  	   
	asignacion FIN_SENTENCIA
    {
        sentenciaIndice = asignacionIndice;    
    }|
    iteracion
    {
        sentenciaIndice = iteracionIndice;
    }|
    seleccion 
    {
        sentenciaIndice = seleccionIndice;
    }|
    salida FIN_SENTENCIA 
    {
        sentenciaIndice = salidaIndice;
    }|
    entrada FIN_SENTENCIA
    {
        sentenciaIndice = entradaIndice;
    }|
    decvar;

decvar:
        RES_DIM OP_COMP_MENOR variables OP_COMP_MAYOR RES_AS OP_COMP_MENOR listatipos OP_COMP_MAYOR 
    {   
        if(cantVariables > cantTipos){
            printf("\n\nERROR! Faltan tipos para las variables declaradas.\n");
            exit(0);
        }
        if(cantVariables < cantTipos){
            printf("\n\nERROR! Excesivos tipos para las variables declaradas.\n");
            exit(0);
        }
        printf("\nDIM OP_COMP_MENOR variables OP_COMP_MAYOR RES_AS OP_COMP_MENOR listatipos OP_COMP_MAYOR");
        asignarTipo(&pilaTIPO);
    }; 

variables:
    ID { 
        if(existeVariable($1) == 0){
            printf("\n\nERROR! Variable Duplicada.\n");
            exit(0);
        } 
        cantVariables++;
        cargarEnTS($1, 7);
        } | variables COMA ID {
            if(existeVariable($3) == 0){
                printf("\n\nERROR! Variable Duplicada.\n");
                exit(0);
            }
            cantVariables++; 
            cargarEnTS($3, 7);
        };

listatipos:
    tipodato {cantTipos++;} | listatipos COMA tipodato {cantTipos++;};

tipodato:
    RES_TIPO_ENTERO {int valor1 = 1;apilar(&pilaTIPO, &valor1);}; |
    RES_TIPO_REAL {int valor3 = 3;apilar(&pilaTIPO, &valor3);}; |
    RES_TIPO_STRING{int valor5 = 5;apilar(&pilaTIPO, &valor5);};;  

salida:
    RES_PUT mensaje
    {
        printf("\nPUT mensaje");
        salidaIndice =CargarEnTercetos("PUT",ConvertirAString(mensajeIndice), "");
    };

 mensaje:
    ID
    {
        cargarEnTS($1, 7);
        mensajeIndice = CargarEnTercetos($1, "", "");
    } |
    CTE_STRING
    {
        cargarEnTS($1, 6);
        mensajeIndice = CargarEnTercetos($1, "", "");
    };

 entrada:   
    RES_GET mensaje
    {
        printf("\nGET mensaje");
        entradaIndice =CargarEnTercetos("GET", ConvertirAString(mensajeIndice), "");
    };

asignacion:
    RES_CONST ID OP_ASIG constante
    {
        cargarEnTS($2, tipoConstante);
        printf("\nCONST ID OP_ASIG constante");
        indiceConst = CargarEnTercetos($2, "", "");
        CargarEnTercetos(":=", ConvertirAString(indiceConst), ConvertirAString(constanteIndice));
    } |
    ID { if(existeVariable($1) != 0){
            printf("\n\nERROR! Variable usada pero no declarada.\n");
            exit(0);
        } 
        cargarEnTS($1, 7);
        idIndice = CargarEnTercetos($1, "", ""); 
        } OP_ASIG expresion
    {
        tipoID=buscarTipoID($1);
        /*if( tipoID != tipoExpresion )
        {
            if((tipoID == 8 && tipoExpresion == 1) || (tipoID == 9 && tipoExpresion == 3) || (tipoID == 10 && tipoExpresion == 5) ||
               (tipoID == 8 && tipoExpresion == 3) || (tipoID == 9 && tipoExpresion == 1)){
                   break;
               }
            printf("\n\nERROR! Asignacion de tipos no valida.\n");
            exit(0); 
        }*/
        printf("\nID OP_ASIG expresion"); 
        asignacionIndice =CargarEnTercetos(":=", ConvertirAString(idIndice), ConvertirAString(expresionIndice));
    };

iteracion:
    {
        iteracionIndice = CargarEnTercetos("ET", "","");
        apilar(&pilaWHILE, &iteracionIndice);
    }
    RES_WHILE PAR_ABRE condicionWhile PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA
    {
        printf("\nWHILE PAR_ABRE condicionWhile PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA");
        iteracionIndice = CargarEnTercetos("BI", "","");
        int valor1 = 0;
        desapilar(&pilaWHILE, &valor1);
        CargarEnTercetoValorDesapilado(valor1, ConvertirAString(iteracionIndice + 1));
        int valor2 = 0;
        desapilar(&pilaWHILE, &valor2);
        CargarEnTercetoValorDesapilado(iteracionIndice, ConvertirAString(valor2));
    };

seleccion:
    RES_IF PAR_ABRE condicionIf PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA 
    {
        programaIndice = CargarEnTercetos("BI", "","");
        int valor = 0;
        desapilar(&pilaIF, &valor);
        CargarEnTercetoValorDesapilado(valor, ConvertirAString(programaIndice + 1));
        apilar(&pilaIF, &programaIndice);
        printf("\nIF PAR_ABRE condicion PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA ELSE BLOQUE_ABRE programa BLOQUE_CIERRA");
    } bloque_else |    
    RES_IF PAR_ABRE condicionIf PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA
    {
        seleccionIndice = programaIndice;
        int valor = 0;
        desapilar(&pilaIF, &valor);
        CargarEnTercetoValorDesapilado(valor, ConvertirAString(programaIndice + 1));
        printf("\nIF PAR_ABRE condicion PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA");
    };|
    RES_IF PAR_ABRE condicionIf PAR_CIERRA sentencia 
    {
        seleccionIndice = sentenciaIndice;
        int valor = 0;
        desapilar(&pilaIF, &valor);
        CargarEnTercetoValorDesapilado(valor, ConvertirAString(sentenciaIndice + 1));
        printf("\nIF PAR_ABRE condicion PAR_CIERRA sentencia");
    };|
    RES_IF PAR_ABRE condicionIf RES_AND condicionIf PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA 
    {
        programaIndice = CargarEnTercetos("BI", "","");
        procesarIfAnd(programaIndice);
        apilar(&pilaIF, &programaIndice);
        printf("\nIF PAR_ABRE AND PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA ELSE BLOQUE_ABRE programa BLOQUE_CIERRA");
    } bloque_else |    
    RES_IF PAR_ABRE condicionIf RES_AND condicionIf PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA
    {
        seleccionIndice = programaIndice;
        procesarIfAnd(programaIndice);
        printf("\nIF PAR_ABRE AND PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA");
    };|
    RES_IF PAR_ABRE condicionIf RES_AND condicionIf PAR_CIERRA sentencia 
    {
        seleccionIndice = sentenciaIndice;
        procesarIfAnd(sentenciaIndice);
        printf("\nIF PAR_ABRE AND PAR_CIERRA sentencia");
    };|
    RES_IF PAR_ABRE condicionIf RES_OR condicionIf PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA 
    {
        programaIndice = CargarEnTercetos("BI", "","");
        seleccionIndice = programaIndice;
        procesarIfOr(programaIndice, comparadorIndice);
        apilar(&pilaIF, &programaIndice);
        printf("\nIF PAR_ABRE OR PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA ELSE BLOQUE_ABRE programa BLOQUE_CIERRA");
    } bloque_else |    
    RES_IF PAR_ABRE condicionIf RES_OR condicionIf PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA
    {
        seleccionIndice = programaIndice;
        procesarIfOr(programaIndice, comparadorIndice);
        printf("\nIF PAR_ABRE OR PAR_CIERRA BLOQUE_ABRE programa BLOQUE_CIERRA");
    };|
    RES_IF PAR_ABRE condicionIf RES_OR condicionIf PAR_CIERRA sentencia 
    {
        seleccionIndice = sentenciaIndice;
        procesarIfOr(sentenciaIndice, comparadorIndice);
        printf("\nIF PAR_ABRE OR PAR_CIERRA sentencia");
    };


bloque_else:
    RES_ELSE BLOQUE_ABRE programa BLOQUE_CIERRA 
    {
        int valor = 0;
        desapilar(&pilaIF, &valor);
        CargarEnTercetoValorDesapilado(valor, ConvertirAString(programaIndice + 1));
    };

condicionIf:
    comparacion 
    {
        apilar(&pilaIF, &comparadorIndice);
        //condicionIndice = CargarEnTercetos(ConvertirAString(comparadorIndice), "", "");
    };

condicionWhile:
    comparacion 
    {
        apilar(&pilaWHILE, &comparadorIndice);
        //condicionIndice = CargarEnTercetos(ConvertirAString(comparadorIndice), "", "");
    }|
    /*condicionWhile RES_AND comparacion  {printf("\ncondicionWhile AND comparacion");};|
    condicionWhile RES_OR comparacion  {printf("\ncondicionWhile OR comparacion");};
    */

comparacion:
    ladoizquierdo comparador ladoderecho
    {
        comparacionIndice = CargarEnTercetos("CMP", ConvertirAString(lizquierdoIndice), ConvertirAString(lderechoIndice));
        comparadorIndice = CargarEnTercetos(tipoComparador, "", "");
    }|
    RES_NOT ladoizquierdo comparador ladoderecho
    {
        comparacionIndice = CargarEnTercetos("CMP", ConvertirAString(lizquierdoIndice), ConvertirAString(lderechoIndice));
        comparadorIndice = CargarEnTercetos(tipoComparador, "", "");
    }|
    RES_NOT ladoizquierdo
    {
        comparacionIndice = CargarEnTercetos("CMP", ConvertirAString(lizquierdoIndice), "0");
        comparadorIndice = CargarEnTercetos("BNE", "", "");
    }|
    ladoizquierdo
    {
        comparacionIndice = CargarEnTercetos("CMP", ConvertirAString(lizquierdoIndice), "0");
        comparadorIndice = CargarEnTercetos("BQE", "", "");
    };
   

ladoizquierdo:
    expresion
    {
        lizquierdoIndice = expresionIndice;
    };

ladoderecho:
    expresion
    {
        lderechoIndice = expresionIndice;
    };

comparador:
    OP_COMP_IGUAL 
    {
        strcpy(tipoComparador, "BNE");
    };|
    OP_COMP_DIST
    {
        strcpy(tipoComparador, "BQE");
    };|
    OP_COMP_MAYOR
    {
        strcpy(tipoComparador, "BLE");
    };|
    OP_COMP_MAYOR_IGUAL
    {
        strcpy(tipoComparador, "BLT");
    };|
    OP_COMP_MENOR
    {
        strcpy(tipoComparador, "BGE");
    };|
    OP_COMP_MENOR_IGUAL
    {
        strcpy(tipoComparador, "BGT");
    };;

expresion:
    expresion
    {
        expresionIndice1 = expresionIndice;
    } OP_SUMA termino
    {
        if(tipoAux == 5 || tipoExpresion == 5){
            printf("\nERROR: No se puede sumar Strings!\n");
            exit(0);
        }
        expresionIndice = CargarEnTercetos("+", ConvertirAString(expresionIndice1), ConvertirAString(terminoIndice));
    };|
    expresion
    {
        expresionIndice1 = expresionIndice;
    }
    OP_RESTA termino 
    {
        if(tipoAux == 5 || tipoExpresion == 5){
            printf("\nERROR: No se puede restar Strings!\n");
            exit(0);
        }             
        expresionIndice = CargarEnTercetos("-", ConvertirAString(expresionIndice1), ConvertirAString(terminoIndice));
    };|
    termino
    {
        tipoExpresion = tipoTermino;
        expresionIndice = terminoIndice;
    };

lista:
    expresion 
    {
        listaIndice = CargarEnTercetos(":=", ConvertirAString(maximoIndice), ConvertirAString(expresionIndice));
    }|
    lista COMA expresion
    {
        listaIndice = CargarEnTercetos("CMP", ConvertirAString(maximoIndice), ConvertirAString(expresionIndice));
        listaIndice = CargarEnTercetos("BGE", "", "");
        apilar(&pilaMAXIMO, &listaIndice);
        listaIndice = CargarEnTercetos(":=", ConvertirAString(maximoIndice), ConvertirAString(expresionIndice));
        int valor = 0;
        desapilar(&pilaMAXIMO, &valor);
        CargarEnTercetoValorDesapilado(valor, ConvertirAString(listaIndice + 1));
        
    };

termino:
    termino
     {
        terminoIndice1 = terminoIndice;
    }OP_MULT factor 
    {
        if(tipoAux == 5 || tipoTermino == 5){
            printf("\nERROR: No se puede multiplicar Strings!\n");
            exit(0);
        }
        terminoIndice = CargarEnTercetos("*", ConvertirAString(terminoIndice1), ConvertirAString(factorIndice));
    };|
    termino
     {
        terminoIndice1 = terminoIndice;
    }OP_COCIENTE factor 
    {
        if(tipoAux == 5 || tipoTermino == 5){
            printf("\nERROR: No se puede dividir Strings!\n");
            exit(0);
        }
        terminoIndice = CargarEnTercetos("/", ConvertirAString(terminoIndice1), ConvertirAString(factorIndice));
    };|
    factor
    {
        tipoTermino = tipoFactor;
        terminoIndice = factorIndice;
    };

factor:
    maximo 
    {
        tipoFactor = tipoAux;
        factorIndice = maximoIndice;
    }|
    PAR_ABRE expresion PAR_CIERRA 
    {
        tipoFactor = tipoAux;
        factorIndice = expresionIndice;
    };|
    ID 
    {
        tipoFactor = buscarTipoID($1);
        cargarEnTS($1, 7);
        factorIndice = CargarEnTercetos($1, "", "");
    }|
    constante
    {
        tipoFactor = tipoAux;
        factorIndice = constanteIndice;
    };

maximo:
    {
        char *max = obtenerNombreVariable();       
        maximoIndice = CargarEnTercetos(max, "", "");
    }
    RES_MAXIMO PAR_ABRE lista PAR_CIERRA
    {
        printf("\nMAXIMO PAR_ABRE lista PAR_CIERRA");
    };

constante:
    CTE_STRING{
                tipoAux = 5; //Para saber que es CTE STRING
                cargarEnTS($1, 6);
                constanteIndice = CargarEnTercetos($1, "", "");
                tipoConstante=10;
            }|
    CTE_INT {
                tipoAux = 1; //Para saber que es CTE ENTERA
                char valorString[100];
                sprintf(valorString, "%d", $1);
                cargarEnTS(valorString, 2);
                constanteIndice = CargarEnTercetos(valorString, "", "");
                tipoConstante=8;
            }|
    CTE_REAL{
                tipoAux = 3; //Para saber que es CTE REAL
                char valorString[100];
                sprintf(valorString, "%lf", $1);
                cargarEnTS(valorString, 4);
                constanteIndice = CargarEnTercetos(valorString, "", "");
                tipoConstante=9;
            };

%%


int main(int argc, char *argv[])
{
    if((yyin = fopen(argv[1], "rt"))==NULL)
    {
        printf("\nNo se puede abrir el archivo de prueba: %s\n", argv[1]);
    }
    else
    { 
        crear_pila(&pilaIF);
        crear_pila(&pilaWHILE);
        crear_pila(&pilaTIPO);
        crear_pila(&pilaMAXIMO);
        yyparse();
    }
	fclose(yyin);

    return 0;
}

int yyerror(void)
{
    printf("\nError Sintactico\n");
	exit(1);
}

char* ConvertirAString(int indice)
{
    char *strIndice = malloc(100 * sizeof(char));
    strIndice[0] = '\0'; 
    char ind[100];
    sprintf(ind, "%d", indice);
    strcat(strIndice, "[");
    strcat(strIndice, ind);
    strcat(strIndice, "]");

    return strIndice;
}

void procesarIfAnd(int indice)
{
    int valor = 0;
    desapilar(&pilaIF, &valor);
    CargarEnTercetoValorDesapilado(valor, ConvertirAString(indice + 1));
    valor = 0;
    desapilar(&pilaIF, &valor);
    CargarEnTercetoValorDesapilado(valor, ConvertirAString(indice + 1));
}

void procesarIfOr(int indice, int indice2)
{    
    int valor = 0;
    desapilar(&pilaIF, &valor);
    CargarEnTercetoValorDesapilado(valor, ConvertirAString(indice + 1));
    valor = 0;
    desapilar(&pilaIF, &valor);
    negarTerceto(valor);
    CargarEnTercetoValorDesapilado(valor, ConvertirAString(indice2 + 1));
}

char* obtenerNombreVariable()
{
    char *max = malloc(100 * sizeof(char));
    max[0] = '\0';
    char ind[100];
    sprintf(ind, "%d", nroMax);
    strcpy(max, "@max");
    strcat(max, ind);


    return max;
}
